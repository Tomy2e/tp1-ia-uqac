﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{
    class Node
    {
        public Node parent;
        public List<Node> enfants;
        public int depth;
        public int distance;
        public String action;
        public Grille state;

        public Node(Node parent)
        {
            this.parent = parent;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Node;

            if (item == null)
            {
                return false;
            }

            return state.Equals(item.state, true);
        }


    }
}
