﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TP1_IA_UQAC
{
    static class Threads
    {
        public static void CallToRobotThread(Environment env)
        {
            Thread.Sleep(2000);
            Console.WriteLine("hello I'm the robot's thread");

            // Capteur sur la grille affichée à l'écran
            Capteur capteur = env.Form.Grille.Capteur;

            // Effecteur sur la grille affichée à l'écran
            Effecteur effecteur = env.Form.Grille.Effecteur;

            // Liste d'actions à effectuer (vide initialement)
            LinkedList<String> actionSeq = new LinkedList<String>();

            // Objectif à obtenir: une grille sans poussière ni bijoux!
            Grille goal = new Grille(env.Form.Grille.Cols, env.Form.Grille.Rows);

            // Création de l'agent
            Robot agent = new Robot(env, goal);

            while (env.GameIsRunning)
            {
                // Observe
                Grille grilleObservee = capteur.GetCopyOfGrille();

                if (!actionSeq.Any())
                {
                    Node soluce = null;

                    // Choix de l'algorithme de recherche
                    switch(env.Form.SelectedAlgorithm())
                    {
                        case "Iterative Deepening Search":
                            Console.WriteLine("IDS");
                            soluce = agent.IterativeDLS(grilleObservee);
                            break;
                        case "Tree Search":
                            Console.WriteLine("Tree Search");
                            soluce = agent.TreeSearch(grilleObservee, new Queue<Node>());
                            break;

                        case "A*":
                            Console.WriteLine("A*");
                            Node s = new Node(null);
                            s.action = "";
                            s.state = grilleObservee;
                            s.depth = 0;
                            soluce = agent.AStar(s);
                            break;

                        default:
                            Console.WriteLine("No algorithm selected..");
                            break;
                    }

                    // Si une solution est trouvée on récupère les actions à effectuer
                    if(soluce != null)
                    {
                        do
                        {
                            Console.WriteLine(soluce.action);
                            if (soluce.action != "")
                                actionSeq.AddFirst(soluce.action);
                            soluce = soluce.parent;
                        }
                        while (!(soluce is null));
                    }
                    else
                    {
                        Console.WriteLine("no soluce found");
                    }

                    // Mise à jour de l'affichage
                    env.Form.RedrawPanelVisu(capteur.Performance(), actionSeq);
                }
                else
                {
                    // Effectuer les actions
                    String action = actionSeq.First();
                    actionSeq.RemoveFirst();
                    effecteur.EffectuerAction(action);

                    // Mise à jour de l'affichage
                    env.Form.RedrawPanelVisu(capteur.Performance(), actionSeq);
                }
                
                // Délai entre chaque action
                Thread.Sleep(500);
            }

            Console.WriteLine("Robot's thread ended...");
        }

        public static void CallToEnvironmentThread(Environment env)
        {
            Console.WriteLine("Hello I'm the environment's thread");

            while (env.GameIsRunning)
            {
                // Générer une poussière ?
                if(env.ShouldThereBeANewDirtySpace())
                {
                    env.PlacerElement(Grille.POUSSIERE);
                }

                // Générer un bijou ?
                if(env.ShouldThereBeANewLostJewel())
                {
                    env.PlacerElement(Grille.BIJOU);
                }

                // Délai pour ne pas ajouter trop d'éléments rapidement
                Thread.Sleep(500);
            }

            Console.WriteLine("Environment's thread ended...");
        }
    }
}
