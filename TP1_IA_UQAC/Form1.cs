﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace TP1_IA_UQAC
{
    public partial class Form1 : Form
    {
        private Image robotImage;
        private Image dustImage;
        private Image diamondImage;

        private int panelVisuWidth;
        private int panelVisuHeight;

        private int rows;
        private int cols;

        private int rowSize;
        private int colSize;

        public readonly Grille Grille;

        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            // Calcul de la taille des cases
            panelVisuWidth = panelVisu.Size.Width;
            panelVisuHeight = panelVisu.Size.Height;

            rows = 5;
            cols = rows;

            rowSize = panelVisuHeight / rows;
            colSize = panelVisuWidth / cols;

            Grille = new Grille(rows, cols);

            //Grille[0, 0] = 1;
            //Grille[0, 1] = 2;
            //Grille[9, 2] = 3;

            /*Random rnd = new Random(); 

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5;j ++)
                {
                    Grille[i, j] = rnd.Next(1, 4);
                }
            }*/

            Size imageSize = new Size((int) ((colSize - 2)/2), (int) (rowSize - 2)/2);

            // Récupération des images à la bonne taille
            robotImage = resizeImage(Properties.Resources.robot, imageSize);
            dustImage = resizeImage(Properties.Resources.dust, imageSize);
            diamondImage = resizeImage(Properties.Resources.diamond, imageSize);

            // ComboBox default algorithm
            comboBox1.SelectedIndex = 0;

        }

        // Source : https://stackoverflow.com/a/14347746
        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        public void RedrawPanelVisu()
        {
            this.panelVisu.Invalidate();
        }

        public String SelectedAlgorithm()
        {
            String text = "";
            this.Invoke((MethodInvoker)delegate ()
            {
                text = comboBox1.Text;
            });

            return text;
            
        }

        public void RedrawPanelVisu(Dictionary<String, int> perf, LinkedList<String> actions)
        {
            // Run code on main thread!
            MethodInvoker inv = delegate
            {
                this.label2.Text = "Performance = " + perf["Performance"];
                this.label3.Text = "Electricité = " + perf["Electricite"];
                this.label4.Text = "Bijoux aspirés = " + perf["BijouAspire"];
                this.label5.Text = "Bijoux ramassés = " + perf["BijouRamasse"];
                this.label6.Text = "Poussières aspirées = " + perf["PoussiereAspire"];
                this.label7.Text = "Poussières ramassées = " + perf["PoussiereRamasse"];

                richTextBox1.Text = "Séquence: \n";

                foreach (String action in actions)
                    this.richTextBox1.Text += action + " -> ";

                richTextBox1.Text += "Fin";
            };
            this.Invoke(inv);


            RedrawPanelVisu();
        }

        private void panelVisu_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black);

            //SolidBrush sb = new SolidBrush(Color.Red);
            //Rectangle myRectangle = new Rectangle(0, 0, panelWidth - 1, panelHeight - 1);
            //g.DrawRectangle(p, myRectangle);

            // Draw cols
            for (int i = 0; i <= cols; i++)
            {
                int x;
                if (i == cols)
                {
                    x = i * colSize - 1;
                }
                else
                {
                    x = i * colSize;
                }
                g.DrawLine(p, new Point(x, 0), new Point(x, panelVisuHeight));
            }

            // Draw Rows
            for (int i = 0; i <= rows; i++)
            {
                int y;

                if (i == rows)
                {
                    y = i * rowSize - 1;
                }
                else
                {
                    y = i * rowSize;
                }
                g.DrawLine(p, new Point(0, y), new Point(panelVisuWidth - 1, y));
            }

            // For each column
            for(int i = 0; i < cols; i++)
            {
                // For each row
                for(int j = 0; j < rows; j++)
                {
                    int[,,] grilleContent = Grille.GetCopyOfContent();
                    int robotPresent = grilleContent[i, j, 0];
                    int dustPresent = grilleContent[i, j, 1];
                    int diamondPresent = grilleContent[i, j, 2];

                    // Le robot est sur cette case ?
                    if (robotPresent == 1)
                    {
                        g.DrawImage(robotImage, i * colSize + (colSize)/4, j * rowSize);
                    }

                    // Poussière sur cette case ?
                    if(dustPresent == 1)
                    {
                        g.DrawImage(dustImage, i * colSize, j * rowSize + rowSize/2);
                    }

                    // Bijou sur cette case ?
                    if(diamondPresent == 1)
                    {
                        g.DrawImage(diamondImage, i * colSize + colSize/2, j * rowSize + rowSize/2);
                    }
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
