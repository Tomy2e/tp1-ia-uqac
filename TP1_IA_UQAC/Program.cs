﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace TP1_IA_UQAC
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Application params, must be called before environment creation
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Create environment
            Environment env = new Environment();

            // Create threads
            Thread envThread = new Thread(() => Threads.CallToEnvironmentThread(env));
            envThread.Start();

            Thread robotThread = new Thread(() => Threads.CallToRobotThread(env));
            robotThread.Start();

            // Run app (infinite loop)
            Application.Run(env.Form);

            // When app is closed, shut down threads
            env.GameIsRunning = false;
            //envThread.Abort();
            //robotThread.Abort();
        }
    }
}
