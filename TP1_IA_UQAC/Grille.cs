﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{
    public class Grille
    {
        public readonly int Rows;
        public readonly int Cols;

        private readonly int[,,] content;

        public readonly Capteur Capteur;
        public readonly Effecteur Effecteur;

        public Tuple<int, int> PosRobot;

        /* Mesures */
        public int Performance = 0;
        public int Electricite = 0;
        public int BijouAspire = 0;
        public int PoussiereAspire = 0;
        public int BijouRamasse = 0;
        public int PoussiereRamasse = 0;

        public static readonly int ROBOT = 0;
        public static readonly int POUSSIERE = 1;
        public static readonly int BIJOU = 2;


        public Grille(int cols, int rows)
        {
            Rows = rows;
            Cols = cols;

            content = new int[cols, rows, 3];
            Capteur = new Capteur(this);
            Effecteur = new Effecteur(this);
        }

        public Boolean Equals(Grille g, bool layerZero = false)
        {
            if(this.Rows == g.Rows && this.Cols == g.Cols)
            {
                for (int i=0; i < Cols; i++)
                {
                    for (int j = 0; j < Rows; j++)
                    {
                        if(layerZero && this.content[i, j, 0] != g.content[i, j, 0])
                                return false;

                        if(this.content[i,j,1] != g.content[i,j,1] || this.content[i, j, 2] != g.content[i, j, 2])
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            return false;
        }

        private Grille(int cols, int rows, int[,,] content, Tuple<int, int> posRobot)
        {
            Rows = rows;
            Cols = cols;
            this.content = content;
            this.PosRobot = posRobot;

            Capteur = new Capteur(this);
            Effecteur = new Effecteur(this);
        }

        public int[,,] GetCopyOfContent()
        {
            return (int[,,])content.Clone();
        }

        public Grille SelfCopy()
        {
            Grille copy = new Grille(Rows, Cols, GetCopyOfContent(), PosRobot);
            return copy;
        }

        public bool EstPlace(int col, int row, int type)
        {
            return content[col, row, type] == 1;
        }

        // "bound" check
        public bool EstDedans(int col, int row)
        {
            if (col < 0 || row < 0)
                return false;

            if (col > (Cols - 1) || row > (Rows - 1))
                return false;

            return true;
        }

        public bool PlacerElement(int col, int row, int type, bool simul = false)
        {
            lock(this)
            {
                if (!EstDedans(col, row))
                    return false;

                // On ne peut pas placer un élément s'il existe déjà
                if (EstPlace(col, row, type))
                    return false;

                if(!simul)
                    content[col, row, type] = 1;

                // Update robot position
                if (type == 0 && !simul)
                    PosRobot = new Tuple<int, int>(col, row);

                return true;
            }
        }

        public bool EnleverElement(int col, int row, int type, bool simul = false)
        {
            lock (this)
            {
                if (!EstDedans(col, row))
                    return false;

                // On ne peut pas enlever un élement inexistant...
                if (!EstPlace(col, row, type))
                    return false;

                if(!simul)
                    content[col, row, type] = 0;

                return true;
            }
        }

        public bool DeplacerElement(int col, int row, int type, int colFinal, int rowFinal, bool simul = false)
        {
            lock (this)
            {
                if (!EstDedans(col, row) || !EstDedans(colFinal, rowFinal))
                    return false;

                // Verifie les collisions
                if (!EstPlace(col, row, type) || EstPlace(colFinal, rowFinal, type))
                    return false;

                if(!simul)
                {
                    content[col, row, type] = 0;
                    content[colFinal, rowFinal, type] = 1;
                }

                // Update robot position
                if (type == 0 && !simul)
                    PosRobot = new Tuple<int, int>(colFinal, rowFinal);

                return true;
            }
        }

        public Tuple<int, int> TrouverRobot()
        {
            return PosRobot;
            lock (this)
            {
                for (int i = 0; i < Cols; i++)
                {
                    for (int j = 0; j < Rows; j++)
                    {
                        if (EstPlace(i, j, 0))
                            return new Tuple<int, int>(i, j);
                    }
                }

                throw new Exception("Could not find robot...");
            }
        }
    }
}
