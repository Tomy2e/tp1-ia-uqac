﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{
    public class Capteur
    {
        private Grille grille;
        public Capteur(Grille grille)
        {
            this.grille = grille;
        }

        public Grille GetCopyOfGrille()
        {
            return grille.SelfCopy();
        }

        public List<String> ActionsPossible()
        {
            List<String> list = new List<string>();

            if (grille.Effecteur.MoveUp(true))
                list.Add("up");

            if (grille.Effecteur.MoveDown(true))
                list.Add("down");

            if (grille.Effecteur.MoveLeft(true))
                list.Add("left");

            if (grille.Effecteur.MoveRight(true))
                list.Add("right");

            if (grille.Effecteur.PickUp(1, true))
                list.Add("pick-1");

            if (grille.Effecteur.PickUp(2, true))
                list.Add("pick-2");

            if (grille.Effecteur.CleanUp(true))
                list.Add("clean");

            return list;
        }

        public Dictionary<String, int> Performance()
        {
            Dictionary<String, int> perf = new Dictionary<String, int>
            {
                { "Performance", grille.Performance },
                { "Electricite", grille.Electricite },
                { "BijouAspire", grille.BijouAspire },
                { "PoussiereAspire", grille.PoussiereAspire },
                { "BijouRamasse", grille.BijouRamasse },
                { "PoussiereRamasse", grille.PoussiereRamasse }
            };

            /*Console.WriteLine("performance = " + grille.Performance);
            Console.WriteLine("Electricite = " + grille.Electricite);
            Console.WriteLine("BijouAspire = " + grille.BijouAspire);
            Console.WriteLine("PoussiereAspire = " + grille.PoussiereAspire);
            Console.WriteLine("BijouRamasse = " + grille.BijouRamasse);*/


            return perf;
        }
    }
}
