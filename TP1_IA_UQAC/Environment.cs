﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{

    class Environment
    {
        private Form1 form;

        private Random rnd = new Random();

        private Boolean gameIsRunning = true;

        public Form1 Form { get => form; }
        public bool GameIsRunning { get => gameIsRunning; set => gameIsRunning = value; }

        public Environment()
        {
            form = new Form1();

            PlacerRobot();

            //form.Grille.Effecteur.MoveUp();
            //PlacerElement(1); PlacerElement(1); PlacerElement(2); PlacerElement(1); PlacerElement(1); 
        }

        public Boolean PlacerElement(int elementNumber)
        {
            int row = rnd.Next(0, Form.Grille.Rows);
            int col = rnd.Next(0, Form.Grille.Cols);

            bool success = form.Grille.PlacerElement(col, row, elementNumber);

            if (!success)
                return false;

            Form.RedrawPanelVisu();
            return true;
        }

        public Boolean PlacerRobot()
        {
            int row = rnd.Next(0, Form.Grille.Rows);
            int col = rnd.Next(0, Form.Grille.Cols);

            bool success = form.Grille.PlacerElement(col, row, Grille.ROBOT);

            if (!success)
                return false;

            Form.RedrawPanelVisu();
            return true;
        }

        public Boolean RandomDecision(int chance)
        {
            return rnd.Next(0, 101) < chance;
        }

        public Boolean ShouldThereBeANewDirtySpace()
        {
            return RandomDecision(3);
        }

        public Boolean ShouldThereBeANewLostJewel()
        {
            return RandomDecision(3);
        }
    }
}
