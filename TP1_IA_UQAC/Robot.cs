﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{
    class Robot
    {
        private Environment environment;

        private List<Node> discover = new List<Node>();
        private List<Node> locked = new List<Node>();

        private Node begin = null;
        private Node ending = null;
        private Node current = null;

        private Grille goal;

        public Robot(Environment environment, Grille goal)
        {
            this.environment = environment;

            this.goal = goal;
        }

        private List<Node> Expand(Node node, Grille problem)
        {
            List<Node> succesors = new List<Node>();

            foreach (String action in node.state.Capteur.ActionsPossible())
            {
                Node s = new Node(node);
                s.action = action;
                s.state = node.state.Capteur.GetCopyOfGrille();
                s.state.Effecteur.EffectuerAction(s.action);
                s.depth = node.depth + 1;
                succesors.Add(s);
            }

            //node.state = null; // memory optimization for TreeSearch... do not use with DLS

            return succesors;
        }

        public Node TreeSearch(Grille problem, Queue<Node> fringe)
        {
            // First node (initial state)
            Node s = new Node(null);
            s.action = "";
            s.state = problem.Capteur.GetCopyOfGrille();
            s.depth = 0;

            fringe.Enqueue(s);

            for(; ;)
            {
                if (!fringe.Any())
                    throw new Exception("Could not find a solution...");

                Node currentNode = fringe.Dequeue();

                // Goal Test
                if (GoalTest(currentNode.state))
                    return currentNode;

                foreach (Node n in Expand(currentNode, problem))
                    fringe.Enqueue(n);
            }

            throw new Exception("todo...");
        }

        public Node getMinVertice()
        {
            
            if (discover.Count > 0)
            {
                Node node = discover[0];
                for (int i = 0; i < discover.Count; i++)
                {
                    if (discover[i].depth + discover[i].distance < node.depth + node.distance)
                    {
                        node = discover[i];
                        //discover.
                    }
                }
                discover.Remove(node);
                locked.Add(node);
                return node;
            }
            return null;
        }

        public List<Grille> GetNeightbors(Grille grid)
        {
            return null;
        }

        public int Distance(Grille grid)
        {
            int count = 0;
            for (int i = 0; i < grid.Cols; i++)
            {
                for (int j = 0; j < grid.Rows; j++)
                {
                    if (grid.GetCopyOfContent()[i, j, 1] == 1 || grid.GetCopyOfContent()[i, j, 2] == 1)
                    {
                        count += 1;
                    }
                }
            }

            return count;
        }

        /*public int Heuristique(Grille grid)
        {
            int count = 0;
            Tuple<int, int> first = null;

            for (int i = 0; i < grid.Cols; i++)
            {
                for (int j = 0; j < grid.Rows; j++)
                {
                    if (grid.GetCopyOfContent()[i, j, 1] == 1 || grid.GetCopyOfContent()[i, j, 2] == 1)
                    {
                        if (first == null)
                            first = new Tuple<int, int>(i, j);
                        count += 1;
                    }
                }
            }

            if(first != null)
            {
                count += Math.Abs(first.Item1 - grid.PosRobot.Item1) + Math.Abs(first.Item2 - grid.PosRobot.Item2);
            }

            return count;
        }*/

        public Node AStar(Node b)
        {
            this.begin = b;
            List<Node> neighbors = new List<Node>();// = null;
            discover.Clear();
            locked.Clear();

            discover.Add(begin);

            current = begin;


            while (Distance(current.state) > 0)
            {

                current = this.getMinVertice();

                if (Distance(current.state) == 0)
                {
                    break;
                }

                //locked.add(current);

                neighbors = this.Expand(current, goal);

                foreach(Node element in neighbors)
                {
                    if (!locked.Contains(element))
                    {
                        if (!discover.Contains(element) && !locked.Contains(element))
                        {
                            discover.Add(element);
                            //element.setPrevious(current);
                            element.distance = Distance(element.state);
                        }
                        if (element.depth > (current.depth + 1))
                        {
                            element.depth = current.depth + 1;
                            element.parent = current;
                        }
                    }
                };
            }

            return current;
            //return null;//return ending
        }

        public bool GoalTest(Grille problem)
        {
            return problem.Equals(goal);
        }

        private Node RecursiveDLS(Node node, int limit)
        {
            bool cutoff = false;

            if (GoalTest(node.state))
            {
                return node;

            }
            else if (limit == node.depth)
            {
                // Return cutoff...
                return null;
            }
            else
            {
                // optimisation: on empêche les boucles...
                Node currentCheckNode = node.parent;
                while (currentCheckNode != null)
                {
                    if(node.state.Equals(currentCheckNode.state, true))
                    {
                        return null;
                    }

                    currentCheckNode = currentCheckNode.parent;
                }

                foreach (Node n in Expand(node, null))
                {
                    Node result = RecursiveDLS(n, limit);

                    if (result == null)
                        cutoff = true;
                    else
                        return result;
                }
            }

            return null;
        }

        public Node DepthLimitedSearch(Grille problem, int limit)
        {
            Node n = new Node(null);
            n.action = "";
            n.depth = 0;
            n.state = problem;

            return RecursiveDLS(n, limit);
        }

        public Node IterativeDLS(Grille problem)
        {
            for(int i = 1; true; i++)
            {
                Console.WriteLine("Iteration " + i);
                Node result = DepthLimitedSearch(problem, i);

                if(result != null)
                {
                    return result;
                }
            }
        }
    }
}
