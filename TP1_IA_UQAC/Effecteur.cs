﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP1_IA_UQAC
{
    public class Effecteur
    {
        private readonly Grille grille;

        public Effecteur(Grille grille)
        {
            this.grille = grille;
        }

        public Boolean MoveUp(bool simul = false)
        {
            lock(this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();
                return grille.DeplacerElement(coords.Item1, coords.Item2, 0, coords.Item1, coords.Item2 - 1, simul);
            }
        }

        public Boolean MoveDown(bool simul = false)
        {
            lock (this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();
                return grille.DeplacerElement(coords.Item1, coords.Item2, 0, coords.Item1, coords.Item2 + 1, simul);
            }
        }

        public Boolean MoveRight(bool simul = false)
        {
            lock (this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();
                return grille.DeplacerElement(coords.Item1, coords.Item2, 0, coords.Item1 + 1, coords.Item2, simul);
            }
        }

        public Boolean MoveLeft(bool simul = false)
        {
            lock (this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();
                return grille.DeplacerElement(coords.Item1, coords.Item2, 0, coords.Item1 - 1, coords.Item2, simul);
            }
        }

        public Boolean PickUp(int typeElement, bool simul = false)
        {
            lock (this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();

                // Mesures de performance
                if (!simul)
                {
                    if(grille.EstPlace(coords.Item1, coords.Item2, Grille.POUSSIERE))
                    {
                        grille.PoussiereRamasse++;
                    }
                    else if(grille.EstPlace(coords.Item1, coords.Item2, Grille.BIJOU))
                    {
                        grille.BijouRamasse++;
                        grille.Performance++;
                    }
                }


                return grille.EnleverElement(coords.Item1, coords.Item2, typeElement, simul);
            }
        }

        public Boolean CleanUp(bool simul = false)
        {
            lock (this)
            {
                // On cherche le robot
                Tuple<int, int> coords = grille.TrouverRobot();

                //System.Media.SoundPlayer simpleSound = new System.Media.SoundPlayer(@"d:\vac.wav");
                //simpleSound.Play();

                // Mesures de performance
                if (!simul)
                {
                    if (grille.EstPlace(coords.Item1, coords.Item2, Grille.POUSSIERE))
                        grille.PoussiereAspire++;
                    else if (grille.EstPlace(coords.Item1, coords.Item2, Grille.BIJOU))
                    {
                        grille.BijouAspire++;
                        grille.Performance--; // Bijou aspiré
                    }

                    // On n'augmente que la performance si le robot aspire une poussiere et pas de bijou
                    if (grille.EstPlace(coords.Item1, coords.Item2, Grille.POUSSIERE) && !grille.EstPlace(coords.Item1, coords.Item2, Grille.BIJOU))
                        grille.Performance++;
                }

                return grille.EnleverElement(coords.Item1, coords.Item2, Grille.POUSSIERE, simul) || grille.EnleverElement(coords.Item1, coords.Item2, Grille.BIJOU, simul);
            }
        }

        public Boolean EffectuerAction(String action)
        {
            bool success = false;

            switch (action)
            {
                case "up":
                    success = MoveUp();
                    break;

                case "down":
                    success = MoveDown();
                    break;

                case "left":
                    success = MoveLeft();
                    break;

                case "right":
                    success = MoveRight();
                    break;

                case "pick-1":
                    success = PickUp(Grille.POUSSIERE);
                    break;

                case "pick-2":
                    success = PickUp(Grille.BIJOU);
                    break;

                case "clean":
                    success = CleanUp();
                    break;

                default:
                    throw new Exception("Unknown action: " + action);
            }

            if (success)
                grille.Electricite++;

            return success;
        }
    }
}
